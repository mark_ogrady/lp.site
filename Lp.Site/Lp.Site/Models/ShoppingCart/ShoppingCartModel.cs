﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lp.Site.Models.ShoppingCart
{
    public class ShoppingCartModel
    {
        public ShoppingCartModel()
        {
            ShoppingCartItems = new List<ShoppingCartItem>();
        }
        public List<ShoppingCartItem> ShoppingCartItems { get; set; }

        public decimal TotalPrice { get; set; }
    }

    public class ShoppingCartItem
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal TotalPrice { get; set; }

    }
}