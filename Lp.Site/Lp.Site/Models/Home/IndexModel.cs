﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MS.Internal.Xml.XPath;

namespace Lp.Site.Models.Home
{
    public class IndexModel
    {
        public IndexModel()
        {
            ProductItems = new List<ProductItem>();
            Slides = new List<Slide>();
        }
        public List<ProductItem> ProductItems { get; set; }

        public List<Slide> Slides { get; set; }
    }

    public class Slide
    {
        public Slide()
        {
            SlideElements = new List<SlideElement>();
        }

        public int SlideId { get; set; }
        public string BackgroundUrl { get; set; }
        public string ThumbUrl { get; set; }
        public List<SlideElement> SlideElements { get; set; }
    }

    public class SlideElement
    {
        public string ElementClass { get; set; }
        public string DataX { get; set; }
        public string DataY { get; set; }
        public string DataSpeed { get; set; }
        public string DataStart { get; set; }
        public string DataEasing { get; set; }
        public string Caption { get; set; }
        public string ImageUrl { get; set; }
    }

    public class ProductItem
    {
        public int ProductId { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string MainImageUrl { get; set; }
        public string ProductName { get; set; }
        public string ProductTagline { get; set; }

    }
}