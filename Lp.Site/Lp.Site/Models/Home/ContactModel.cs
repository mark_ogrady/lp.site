﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Lp.Site.Models.Home
{
    public class ContactModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        public string  Name { get; set; }

        public string Message { get; set; }
    }
}