﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lp.Site.Framework.DataContext;
using Lp.Site.Models.Home;

namespace Lp.Site.Controllers
{
    public class HomeController : Controller
    {
        private LpContext context = new LpContext();

        public ActionResult Index()
        {
            var model = new Lp.Site.Models.Home.IndexModel();
            //find each product
            model.ProductItems = (from p in context.Products
                                  where !p.IsDeleted
                                  select new ProductItem()
                                      {
                                          ProductId = p.ProductId,
                                          ProductName = p.ProductName,
                                          MainImageUrl = p.MainImageUrl,
                                          ProductTagline = p.ProductTagline,
                                          ThumbnailImageUrl = p.ThumbnailImageUrl

                                      }).ToList();

            model.Slides = (from s in context.Slides
                            select new Slide()
                                {
                                    BackgroundUrl = s.BackgroundUrl,
                                    ThumbUrl = s.ThumbUrl,
                                    SlideId =  s.SlideId

                                }).ToList();

            foreach (var slide in model.Slides)
            {
                slide.SlideElements = (from se in context.SlideElements
                                       where se.Slide.SlideId == slide.SlideId
                                       select new SlideElement()
                                           {
                                               Caption = se.Caption,
                                               DataEasing = se.DataEasing,
                                               DataSpeed = se.DataSpeed,
                                               DataStart = se.DataStart,
                                               DataX = se.DataX,
                                               DataY = se.DataY,
                                               ElementClass = se.ElementClass,
                                               ImageUrl = se.ImageUrl

                                           }
                                      ).ToList();
            }


            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            var model = new ContactModel();
            

            return View(model);
        }

        [HttpPost]
        public ActionResult Contact(ContactModel model)
        {


            return View();
        }
    }
}
