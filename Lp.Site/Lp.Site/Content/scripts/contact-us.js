var ContactUs = function () {

    return {
        //main function to initiate the module
        init: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				lat: 52.2474,
				lng: 0.7183,
			  });
			   var marker = map.addMarker({
			       lat: 52.2474,
			       lng: 0.7183,
		            title: 'little parsley Limited',
		            infoWindow: {
		                content: "<b>little parsley.</b> Bury St Edmunds<br>Suffolk"
		            }
		        });

			   marker.infoWindow.open(map, marker);
			});
        }
    };

}();