﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Site.Framework.Model
{
    public class SlideElement
    {
        public int SlideElementId { get; set; }
        public virtual Slide Slide { get; set; }
        public string ElementClass { get; set; }
        public string DataX { get; set; }
        public string DataY { get; set; }
        public string DataSpeed { get; set; }
        public string DataStart { get; set; }
        public string DataEasing { get; set; }
        public string Caption { get; set; }
        public string ImageUrl { get; set; }
    }
}
