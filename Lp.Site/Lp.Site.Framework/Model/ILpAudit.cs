﻿using System;

namespace Lp.Site.Framework.Model
{
    public interface ILpAudit
    {
        bool IsDeleted { get; set; }
        DateTime CreatedDate { get; set; }
        string CreatedUser { get; set; }
        DateTime UpdatedDate { get; set; }
        string UpdatedUser { get; set; }
        decimal Version { get; set; }
    }
}
