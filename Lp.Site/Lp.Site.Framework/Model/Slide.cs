﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lp.Site.Framework.Model
{
    public class Slide : ILpAudit
    {
        public int SlideId { get; set; }

        [Required()]
        public string BackgroundUrl { get; set; }
        public string ThumbUrl { get; set; }
        
        public ICollection<SlideElement> SlideElements { get; set; }
     
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
    }
}
