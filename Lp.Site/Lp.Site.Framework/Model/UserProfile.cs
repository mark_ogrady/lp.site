﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lp.Site.Framework.Model
{
    [Table("UserProfile")]
    public class UserProfile 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

       
        public ICollection<Role> Roles { get; set; }


        
    }
}
