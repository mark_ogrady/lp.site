﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lp.Site.Framework.Model
{
    public class Product :ILpAudit
    {
        [Key]
        public int ProductId { get; set; }

       
        public string ThumbnailImageUrl { get; set; }
        public string MainImageUrl { get; set; }
        public string ProductName { get; set; }
        public string ProductTagline { get; set; }
        public string Description { get; set; }
        

        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedUser { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public decimal Version { get; set; }
    }
}
