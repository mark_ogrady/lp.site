﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lp.Site.Framework.DataContext;
using Lp.Site.Framework.Model;

namespace Lp.Site.Framework.Seed.Slider
{
    public static class SeedSlider
    {
        public static void CreateSlides(LpContext context)
        {

            var slides = new List<Model.Slide>();

            slides.Add(new Slide()
            {
                SlideId = 1,
                BackgroundUrl = "/Content/img/sliders/revolution/bg1.jpg",
                ThumbUrl = "/Content/img/sliders/revolution/thumbs/thumb2.jpg",
            });

            slides.Add(new Slide()
            {
                SlideId = 2,
                BackgroundUrl = "/Content/img/sliders/revolution/bg2.jpg",
                ThumbUrl = "/Content/img/sliders/revolution/thumbs/thumb2.jpg",
            });

            slides.Add(new Slide()
            {
                SlideId = 3,
                BackgroundUrl = "/Content/img/sliders/revolution/bg3.jpg",
                ThumbUrl = "/Content/img/sliders/revolution/thumbs/thumb2.jpg",
            });

            foreach (var slide in slides)
            {
                var slideExists = context.Slides.FirstOrDefault(o => o.SlideId == slide.SlideId);
                if (slideExists == null)
                {
                    context.Slides.Add(slide);

                }
                context.SaveChanges();
            }

            //Add Elements

            var slideElements = new List<Model.SlideElement>();

            #region 1st Elements
            
            slideElements.Add(new SlideElement()
            {
                SlideElementId = 1,
                Slide = slides.FirstOrDefault(o => o.SlideId == 1),
                Caption= "Sage Dev",
                DataX = "0",
                DataY = "125",
                DataSpeed = "400",
                DataStart = "1500",
                DataEasing = "easeOutExpo"
            });

            slideElements.Add(new SlideElement()
            {
                SlideElementId = 1,
                Slide = slides.FirstOrDefault(o => o.SlideId == 1),
                Caption = "New Addons",
                DataX = "0",
                DataY = "180",
                DataSpeed = "400",
                DataStart = "2000",
                DataEasing = "easeOutExpo"
            });

            slideElements.Add(new SlideElement()
            {
                SlideElementId = 1,
                Slide = slides.FirstOrDefault(o => o.SlideId == 1),
                Caption = "Caption 3",
                DataX = "0",
                DataY = "225",
                DataSpeed = "400",
                DataStart = "2500",
                DataEasing = "easeOutExpo"
            });


            slideElements.Add(new SlideElement()
            {
                SlideElementId = 1,
                Slide = slides.FirstOrDefault(o => o.SlideId == 1),
                Caption = "",
                DataX = "0",
                DataY = "125",
                DataSpeed = "400",
                DataStart = "1500",
                DataEasing = "easeOutExpo",
           
                
            });

            
            #endregion



            foreach (var slideElement in slideElements)
            {
                var slideElementExists = context.SlideElements.FirstOrDefault(o => o.SlideElementId == slideElement.SlideElementId);
                if (slideElementExists == null)
                {
                    context.SlideElements.Add(slideElement);
                }
                context.SaveChanges();
            }


        }
    }
}
