﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lp.Site.Framework.DataContext;

namespace Lp.Site.Framework.Seed.Product
{
    public static class SeedProduct
    {
        public static void CreateProducts(LpContext context)
        {
            var productList = new List<Model.Product>();

            for (int i = 1; i < 4; i++)
            {
                productList.Add(new Model.Product()
                {
                    ProductName = "Product " + i.ToString(),
                    ProductTagline = "Product " + i.ToString() + "tag",
                    MainImageUrl = "/Content/img/works/img" + i.ToString() +".jpg",
                    Description = "Product Description",
                    ThumbnailImageUrl = ""
                });
            }
            foreach (var product in productList)
            {
                var existingMenu = context.Products.FirstOrDefault(p => p.ProductName == product.ProductName);
                if (existingMenu == null)
                    context.Products.Add(product);
            }
            context.SaveChanges();
            

        }
    }
}
