﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Lp.Site.Framework.Model;

namespace Lp.Site.Framework.DataContext
{
    public class LpContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Membership> Membership { get; set; }
        public DbSet<Slide> Slides { get; set; }
        public DbSet<SlideElement> SlideElements { get; set; }


        public LpContext()
            : base("LpConnection")
        {

        }

        static LpContext()
        {
            Database.SetInitializer(new LpDatabaseInitialiser());
        }

        public override int SaveChanges()
        {
            ApplyAudit();
            return base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Configurations.Add(new RoleConfiguration());
            modelBuilder.Entity<UserProfile>().HasMany<Role>(r => r.Roles).WithMany(u => u.UserProfiles).Map(m =>
            {
                m.ToTable("webpages_UsersInRoles");
                m.MapLeftKey("UserId");
                m.MapRightKey("RoleId");
            });
        }

        public void ApplyAudit()
        {
            foreach (var entry in this.ChangeTracker.Entries()
                                      .Where(
                                          e => e.Entity is ILpAudit &&
                                               (e.State == EntityState.Added) ||
                                               (e.State == EntityState.Modified)))
            {
                try
                {
                    ILpAudit e = (ILpAudit)entry.Entity;

                    if (entry.State == EntityState.Added)
                    {
                        e.CreatedDate = DateTime.UtcNow;
                    }

                    e.UpdatedDate = DateTime.UtcNow;
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
