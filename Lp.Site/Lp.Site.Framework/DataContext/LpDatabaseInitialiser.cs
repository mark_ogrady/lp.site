﻿using System.Data.Entity;

namespace Lp.Site.Framework.DataContext
{
    public class LpDatabaseInitialiser :
        //CreateDatabaseIfNotExists<KwContext>      // when model is stable
         DropCreateDatabaseIfModelChanges<LpContext>
    {
        protected override void Seed(LpContext context)
        {
            SeedData(context);
            base.Seed(context);
        }

        public void SeedData(LpContext context)
        {
           Framework.Seed.Product.SeedProduct.CreateProducts(context);
            context.SaveChanges();
        }
    }
}
