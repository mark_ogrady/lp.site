﻿using System;
using Lp.Site.Framework.DataContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lp.Site.Tests.Seed
{
    [TestClass]
    public class SlideSeed
    {
        [TestMethod]
        public void AddSlides()
        {
            using (var context = new LpContext())
            {
                Framework.Seed.Slider.SeedSlider.CreateSlides(context);

            }
        }
    }
}
