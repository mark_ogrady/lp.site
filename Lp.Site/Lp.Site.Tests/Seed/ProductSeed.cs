﻿using System;
using Lp.Site.Framework.DataContext;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lp.Site.Tests.Seed
{
    [TestClass]
    public class ProductSeed
    {
        [TestMethod]
        public void AddProducts()
        {
            using (var context = new LpContext())
            {
                Framework.Seed.Product.SeedProduct.CreateProducts(context);

            }
        }
    }
}
